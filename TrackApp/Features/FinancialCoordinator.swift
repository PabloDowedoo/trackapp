//
//  FinancialCoordinator.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppCommon
import TrackAppDomain

struct FinancialCoordinator {

    private let router: UINavigationController
    private let moduleFactory: FinancialModuleFactoryType
    private let services: FinancialServices

    init(router: UINavigationController,
         moduleFactory: FinancialModuleFactoryType,
         services: FinancialServices) {
        self.router = router
        self.moduleFactory = moduleFactory
        self.services = services
    }

    func start() {
        navigateToDashboard()
    }
}

// MARK: - Horizontal Flow
extension FinancialCoordinator {
    func navigateToDashboard() {
        let vc = moduleFactory.makeDashboard(actions: dashboardActions,
                                             services: services)
        router.pushViewController(vc, animated: true)
    }

    func navigateToTransaction(onCreatedTransaction: @escaping VoidClosure) {
        let vc = moduleFactory.makeTransaction(
            actions: transactionActions(onCreatedTransaction: onCreatedTransaction),
            services: services
        )
        router.pushViewController(vc, animated: true)
    }
}

// MARK: - Actions
extension FinancialCoordinator {
    var dashboardActions: DashboardViewModel.Actions {
        return DashboardViewModel.Actions(onAddTap: navigateToTransaction)
    }

    func transactionActions(onCreatedTransaction: @escaping VoidClosure) -> TransactionViewModel.Actions {
        return TransactionViewModel.Actions(
            done: { [weak router] in
                onCreatedTransaction()
                router?.popViewController(animated: true)
            },
            cancel: { [weak router] in
                router?.popViewController(animated: true)
            })
    }
}
