//
//  DashboardAccountView.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import TrackAppDomain
import TrackAppCommon

struct DashboardAccountViewModel: Equatable {
    let title: String
    let amount: String
}

final class DashboardAccountView: UIView {

    private let titleLabel: UILabel = .titleLabel
    private let amountLabel: UILabel = .amountLabel

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(viewModel: DashboardAccountViewModel) {
        titleLabel.text = viewModel.title
        amountLabel.text = viewModel.amount
    }
}

private extension DashboardAccountView {
    func setupView() {
        backgroundColor = .gray
        let contentStackView = UIStackView(views: [titleLabel, amountLabel],
                                           axis: .horizontal,
                                           spacing: 8)
        addSubview(contentStackView)
        contentStackView.makeLayout {
            $0.fillSuperview(with: UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
        }
    }
}

// MARK: UI Factory
private extension UIView {

    static var amountLabel: UILabel {
        let label = UILabel()
        label.font = Fonts.system(ofSize: .body, weight: .regular)
        label.textColor = Palette.text.primary
        label.textAlignment = .right
        return label
    }

    static var titleLabel: UILabel {
        let label = UILabel()
        label.font = Fonts.system(ofSize: .body, weight: .regular)
        label.textColor = Palette.text.primary
        label.textAlignment = .left
        return label
    }
}
