import RxSwift
import RxSwiftExt
import Foundation
import TrackAppDomain
import TrackAppCommon

protocol TransactionViewModelType {
    func transform(input: TransactionViewModel.Input) -> TransactionViewModel.Output
}

struct TransactionViewModel: TransactionViewModelType {

    struct Input {
        let viewIsLoaded: Observable<Void>
        let done: Observable<Void>
        let cancel: Observable<Void>
        let accountButtonTap: Observable<Void>
        let selectedAccountId: Observable<String>
        let categoryButtonTap: Observable<Void>
        let selectedCategoryId: Observable<String>
        let amount: Observable<String>
        let transactionType: Observable<TransactionView.TransactionType>
    }

    struct Output {
        let actions: Observable<Void>
        let categoryTitle: Observable<String>
        let accountTitle: Observable<String>
        let showPickerAccounts: Observable<PickerViewModel>
        let showPickerCategory: Observable<PickerViewModel>
        let isDoneEnabled: Observable<Bool>
    }

    struct Actions {
        let done: VoidClosure
        let cancel: VoidClosure
    }

    private let actions: Actions
    private let currentDate: () -> Date
    private let services: FinancialServices
    
    // MARK: - Lifecycle
    init(actions: Actions,
         services: FinancialServices,
         currentDate: @escaping () -> Date = { return Date() }) {
        self.actions = actions
        self.services = services
        self.currentDate = currentDate
    }

    // MARK: - TransactionViewModelType Methods.
    func transform(input: Input) -> Output {
        let parameters = fetchInitialData(trigger: input.viewIsLoaded).share(replay: 1)

        let categoryList = Observable.combineLatest(parameters.map({ $0.categories }),
                                                    input.transactionType) {
                                                        (categories: $0, transactionType: $1) }
            .map({ data in
                data.categories.filter({
                    $0.transactionType == data.transactionType.toDomain
                })
            })

        let accountList = parameters.map({ $0.accounts })

        let selectedAccount = input.selectedAccountId
            .withLatestFrom(accountList) { (selectedAccountId: $0, accounts: $1 )}
            .map({ data -> Account? in
                guard let selectedAccountId = Int(data.selectedAccountId) else { return nil }
                return data.accounts.first { $0.id == selectedAccountId }
            })
            .startWith(nil)

        let selectedCategory = Observable.combineLatest(input.selectedCategoryId,
                                                        categoryList) {
                                                            (selectedCategoryId: $0,
                                                             categories: $1 )}
            .map({ data -> TrackAppDomain.Category? in
                guard let selectedCategoryId = Int(data.selectedCategoryId) else { return nil }
                return data.categories.first { $0.id == selectedCategoryId }
            })
            .startWith(nil)

        let accountTitle = selectedAccount
            .map({ selectedAccount -> String in
                guard let selectedAccount = selectedAccount else { return Constans.pleaseSelectAccountTitle }
                return Constans.selectedAccountTitle + " " + selectedAccount.name
            })

        let categoryTitle = selectedCategory
            .map({ selectedCategory -> String in
                guard let selectedCategory = selectedCategory else { return Constans.pleaseSelectCategoryTitle}
                return  Constans.selectedCategoryTitle + " " + selectedCategory.name
            })

        let showAccounts = input.accountButtonTap
            .withLatestFrom(Observable.combineLatest(accountList,
                                                     selectedAccount) )
            .map(parseAccountsToPicker(accounts: selectedAccount:))

        let showCategories = input.categoryButtonTap
            .withLatestFrom(Observable.combineLatest(categoryList,
                                                     selectedCategory) )
            .map(parseCategoriesToPicker(categories: selectedCategory:))

        
        let transactionBody = Observable.combineLatest(selectedCategory,
                                                       selectedAccount,
                                                       input.amount) { (category: $0,
                                                                        account: $1,
                                                                        amount: $2) }
            .map({ data -> TransactionBody? in
                guard let categoryId = data.category?.id,
                    let accountId = data.account?.id,
                    let amount = Int(data.amount) else { return nil }
                return TransactionBody(amount: amount,
                                       accountId: accountId,
                                       categoryId: categoryId,
                                       timestamp: self.currentDate().timeIntervalSince1970)
            })
            .startWith(nil)

        let isDoneEnabled = transactionBody
            .map({ return $0 != nil })
            .distinctUntilChanged()

        let done = input.done
            .withLatestFrom(transactionBody)
            .unwrap()
            .flatMap(self.services.createTransaction(transactionBody:))
            .do(onNext: actions.done)

        let cancel = input.cancel.map(actions.cancel)

        let actions = Observable.merge(done, cancel)
        return Output(actions: actions,
                      categoryTitle: categoryTitle,
                      accountTitle: accountTitle,
                      showPickerAccounts: showAccounts,
                      showPickerCategory: showCategories,
                      isDoneEnabled: isDoneEnabled)
    }
}

//MARK: - Private methods
private extension TransactionViewModel {
    func fetchInitialData(trigger: Observable<Void>) -> Observable<(accounts: [Account], categories: [TrackAppDomain.Category])> {
        return trigger
            .flatMapLatest ({ _ -> Single<(accounts: [Account], categories: [TrackAppDomain.Category])> in
                return Single.zip(self.services.getAccountList(),
                                  self.services.getCategoryList()) { (accounts: $0, categories: $1) }
            })
    }

    func parseAccountsToPicker(accounts: [Account],
                               selectedAccount: Account?) -> PickerViewModel {
        let datasource = accounts
            .map ({
                PickerDatasource(id: String($0.id), name: $0.name)
            })
        return PickerViewModel(datasource: datasource, selectedId: (selectedAccount?.id).toString)
    }

    func parseCategoriesToPicker(categories: [TrackAppDomain.Category],
                                 selectedCategory: TrackAppDomain.Category?) -> PickerViewModel {
        let datasource = categories
            .map ({
                PickerDatasource(id: String($0.id), name: $0.name)
            })
        return PickerViewModel(datasource: datasource, selectedId: (selectedCategory?.id).toString)
    }

    enum Constans {
        static var selectedCategoryTitle: String { "Category Selected:" }
        static var selectedAccountTitle: String { "Account Selected:" }
        static var pleaseSelectAccountTitle: String { "Select an account" }
        static var pleaseSelectCategoryTitle: String { "Select a category" }

    }
}

private extension TransactionView.TransactionType {
    var toDomain: TransactionType {
        switch self {
        case .expense:
            return .expense
        case .income:
            return .income
        }
    }
}

extension Optional where Wrapped == Int {
    var toString: String? {
        guard let self = self else { return nil }
        return String(self)
    }
}

extension Optional where Wrapped == String {
    var orEmpty: String {
        return self ?? ""
    }
}
