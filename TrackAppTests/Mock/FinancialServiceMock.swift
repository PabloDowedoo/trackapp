//
//  MockFinancialServices.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 29/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RxSwift
import Foundation
import TrackAppDomain
@testable import TrackApp

class FinancialServicesMock: FinancialServices {

    struct FetchAccountsDetailsParameters {
        var timesCalled = 0
        var response: Single<[AccountDetails]>?
    }

    var fetchAccountDetailsParameters = FetchAccountsDetailsParameters()
    func fetchAccountsDetails() -> Single<[AccountDetails]> {
        fetchAccountDetailsParameters.timesCalled += 1
        return fetchAccountDetailsParameters.response ?? .never()
    }

    struct DeleteTransactionParameters {
        var timesCalled = 0
        var id: Int?
        var response: Single<Void>?
    }

    var deleteTransactionParameters = DeleteTransactionParameters()
    func deleteTransaction(id: Int) -> Single<Void> {
        deleteTransactionParameters.id = id
        deleteTransactionParameters.timesCalled += 1
        return deleteTransactionParameters.response ?? .never()
    }

    struct GetAccountListParameters {
        var timesCalled = 0
        var response: Single<[Account]>?
    }

    var getAccountParameters = GetAccountListParameters()
    func getAccountList() -> Single<[Account]> {
        getAccountParameters.timesCalled += 1
        return getAccountParameters.response ?? .never()
    }

    struct GetCategoryListParameters {
        var timesCalled = 0
        var response: Single<[TrackAppDomain.Category]>?
    }

    var getCategoryListParameters = GetCategoryListParameters()
    func getCategoryList() -> Single<[TrackAppDomain.Category]> {
        getCategoryListParameters.timesCalled += 1
        return getCategoryListParameters.response ?? .never()
    }

    struct CreateTransactionParameters {
        var timesCalled = 0
        var transactionBody: TransactionBody?
        var response: Single<Void>?
    }

    var createTransactionParameters = CreateTransactionParameters()
    func createTransaction(transactionBody: TransactionBody) -> Single<Void> {
        createTransactionParameters.timesCalled += 1
        createTransactionParameters.transactionBody = transactionBody
        return createTransactionParameters.response ?? .never()
    }
}

