//
//  XCTestCase+SnapshotTesting.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import XCTest
import SnapshotTesting

extension XCTestCase {
    func snapshotCell(_ cell: UITableViewCell, testName: String = #function, file: StaticString = #file, line: UInt = #line) {
        let viewController = MockTableViewController()
        viewController.view.frame = UIScreen.main.bounds
        viewController.tableView.separatorStyle = .none
        viewController.cells = [cell]
        let indexPath = IndexPath(row: 0, section: 0)
        assertSnapshot(matching: viewController.tableView.cellForRow(at: indexPath)!, as: .image, file: file, testName: testName, line: line)
    }
}
