//
//  MockTableViewController.swift
//  TrackAppTests
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

final class MockTableViewController: UITableViewController {

    var cells: [UITableViewCell] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
}
