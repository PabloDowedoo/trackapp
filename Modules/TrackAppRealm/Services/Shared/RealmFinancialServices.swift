//
//  RealmFinancialServices.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RxSwift
import Foundation
import RealmSwift
import TrackAppDomain

struct RealmFinancialServices: FinancialServices {

    private let realm: Realm

    init(realm: Realm = RealmProvider.shared) {
        self.realm = realm
    }

    func fetchAccountsDetails() -> Single<[AccountDetails]> {
        return .just(realm.objects(AccountRealm.self)
            .map({
                $0.toAccountDetails
            }))
    }

    func deleteTransaction(id: Int) -> Single<Void> {
        let itemToDelete = realm.object(ofType: TransactionRealm.self, forPrimaryKey: id)
        guard let itemToDeleteUnwrapped = itemToDelete else {
            // Return Error
            return .just(())
        }
        realm.safeWrite {
            realm.delete(itemToDeleteUnwrapped)
        }
        return .just(())
    }

    func getAccountList() -> Single<[Account]> {
        return .just(realm.objects(AccountRealm.self)
            .map({
                $0.toAccount
            }))
    }

    func getCategoryList() -> Single<[TrackAppDomain.Category]> {
        return .just(realm.objects(CategoryRealm.self)
            .map({
                $0.toCategory
            }))
    }

    func createTransaction(transactionBody: TransactionBody) -> Single<Void> {
        guard let transaction = TransactionRealm.build(realm: realm,
                                                       categoryId: transactionBody.categoryId,
                                                       amount: transactionBody.amount,
                                                       timestamp: transactionBody.timestamp),
            let accountRealm = realm.object(ofType: AccountRealm.self,
                                                    forPrimaryKey: transactionBody.accountId)
            else {
                // Return Error
                return .just(())
        }

        realm.safeWrite {
            accountRealm.transactions.append(transaction)
        }
        return .just(())
    }
}
