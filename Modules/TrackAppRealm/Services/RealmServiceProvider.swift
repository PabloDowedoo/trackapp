//
//  RealmServiceProvider.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation
import RealmSwift
import TrackAppDomain

public struct RealmServiceProvider: ServiceProvider {

    public static let shared = RealmServiceProvider()

    private init() {
        Realm.Configuration.defaultConfiguration.deleteRealmIfMigrationNeeded = true

        try? FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
        let realm = RealmProvider.shared

        RealmHarcodeDataServices(realm: realm).populateEmptyDatabase()
    }

    public func makeFinancialServices() -> FinancialServices {
        return RealmFinancialServices()
    }
}
