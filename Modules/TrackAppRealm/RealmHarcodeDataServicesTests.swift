//
//  RealmHarcodeDataServices.swift
//  TrackAppRealm
//
//  Created by Pablo Sanchez Gomez on 04/05/2020.
//

import XCTest
import RxSwift
import RealmSwift
@testable import TrackAppRealm

class RealmHarcodeDataServicesTests: XCTestCase {

    var sut: RealmHarcodeDataServices!
    var realmMock: Realm!

    override func setUp() {
        super.setUp()
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        realmMock = try! Realm()
        sut = RealmHarcodeDataServices(realm: realmMock)
    }

    override func tearDown() {
        try! realmMock.write {
             realmMock.deleteAll()
         }
        super.tearDown()
    }

    func test_GivenRealmHarcodeDataServices_WhenPrepopulateEmptyDatabase_ThenReturnPrepopulatedDatabase() {
        XCTAssertEqual(realmMock.objects(CategoryRealm.self).count, 0)
        XCTAssertEqual(realmMock.objects(AccountRealm.self).count, 0)
        XCTAssertEqual(realmMock.objects(TransactionRealm.self).count, 0)

        sut.populateEmptyDatabase()

        XCTAssertEqual(realmMock.objects(CategoryRealm.self).count, 7)
        XCTAssertEqual(realmMock.objects(AccountRealm.self).count, 3)
        XCTAssertEqual(realmMock.objects(TransactionRealm.self).count, 4)
    }
}
