//
//  TransactionRealm.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import RealmSwift
import Foundation
import TrackAppDomain
@objcMembers
class TransactionRealm: Object {
    dynamic var id: Int = 0
    dynamic var amount = RealmOptional<Int>()
    dynamic var timestamp = RealmOptional<Double>()

    dynamic var category: CategoryRealm?

    override static func primaryKey() -> String? { return "\(#keyPath(TransactionRealm.id))" }

    convenience init(id: Int,
                     amount: Int,
                     timestamp: Double,
                     category: CategoryRealm) {
        self.init()
        self.id = id
        self.amount.value = amount
        self.timestamp.value = timestamp
        self.category = category
    }
}

extension TransactionRealm {
    static func incrementalId(realm: Realm) -> Int {
        let primaryKey = "\(#keyPath(TransactionRealm.id))"
        let biggestId = realm.objects(TransactionRealm.self)
            .sorted(byKeyPath: primaryKey)
            .last?
            .id

        return (biggestId ?? 0) + 1
    }
}

extension TransactionRealm {
    static func build(realm: Realm,
                      categoryId: Int,
                      amount: Int,
                      timestamp: Double) -> TransactionRealm? {
        guard let category = realm.object(ofType: CategoryRealm.self, forPrimaryKey: categoryId)
            else {
                return nil
        }
        return TransactionRealm(id: TransactionRealm.incrementalId(realm: realm),
                                amount: amount,
                                timestamp: timestamp,
                                category: category)
    }
}

extension TransactionRealm {
    var transaction: Transaction? {
        guard let category = category else { return nil }
        return Transaction(id: id,
                           amount: amount.value ?? 0,
                           timestamp: timestamp.value ?? 0,
                           category: category.toCategory)
    }
}

