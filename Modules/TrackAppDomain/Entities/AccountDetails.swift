//
//  AccountDetails.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 27/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import Foundation

public struct AccountDetails: Equatable {
    public let id: Int
    public let name: String
    public let transactions: [Transaction]

    public init(id: Int,
                name: String,
                transactions: [Transaction]) {
        self.id = id
        self.name = name
        self.transactions = transactions
    }
}
