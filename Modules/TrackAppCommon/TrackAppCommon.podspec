Pod::Spec.new do |s|
    s.name     = 'TrackAppCommon'
    s.version  = '1.0.0'
    s.summary  = "Utils"
    s.author   = { "TrackAppCommon" => "pabsanmez@gmail.com" }
    s.ios.deployment_target = '12.0'
    s.homepage = "https://github.com/pabsanmez"
    s.source   = { :git => "", :tag => "#{s.version}" }
    s.source_files = '**/*.swift'

    s.dependency 'RxSwift'
    s.dependency 'RxCocoa'
end


