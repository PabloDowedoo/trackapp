//  Created by Albert Montserrat on 25/07/2019.
//  Copyright © 2019 FiNetwork. All rights reserved.
//  All Credits to: https://github.com/AlbertMontserrat/AMGAutolayout

import UIKit

public protocol AnchorContainer {
    var leadingAnchor: NSLayoutXAxisAnchor { get }
    var trailingAnchor: NSLayoutXAxisAnchor { get }
    var leftAnchor: NSLayoutXAxisAnchor { get }
    var rightAnchor: NSLayoutXAxisAnchor { get }
    var topAnchor: NSLayoutYAxisAnchor { get }
    var bottomAnchor: NSLayoutYAxisAnchor { get }
    var widthAnchor: NSLayoutDimension { get }
    var heightAnchor: NSLayoutDimension { get }
    var centerXAnchor: NSLayoutXAxisAnchor { get }
    var centerYAnchor: NSLayoutYAxisAnchor { get }
}

extension UIView: AnchorContainer {}
extension UILayoutGuide: AnchorContainer {}

public class LayoutProxy {

    /// Property for `leadingAnchor`
    public lazy var leading = property(with: view.leadingAnchor)
    /// Property for `trailingAnchor`
    public lazy var trailing = property(with: view.trailingAnchor)
    /// Property for `leftAnchor`
    public lazy var left = property(with: view.leftAnchor)
    /// Property for `rightAnchor`
    public lazy var right = property(with: view.rightAnchor)
    /// Property for `topAnchor`
    public lazy var top = property(with: view.topAnchor)
    /// Property for `bottomAnchor`
    public lazy var bottom = property(with: view.bottomAnchor)
    /// Property for `widthAnchor`
    public lazy var width = dimensionProperty(with: view.widthAnchor)
    /// Property for `heightAnchor`
    public lazy var height = dimensionProperty(with: view.heightAnchor)
    /// Property for `centerXAnchor`
    public lazy var centerX = property(with: view.centerXAnchor)
    /// Property for `centerYAnchor`
    public lazy var centerY = property(with: view.centerYAnchor)

    //Safe area
    /// Property for `topAnchor`. it's related to `safeAreaLayoutGuide`.
    public lazy var safeTop = property(with: view.safeAreaLayoutGuide.topAnchor)
    /// Property for `bottomAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeBottom = property(with: view.safeAreaLayoutGuide.bottomAnchor)
    /// Property for `leftAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeLeft = property(with: view.safeAreaLayoutGuide.leftAnchor)
    /// Property for `rightAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeRight = property(with: view.safeAreaLayoutGuide.rightAnchor)
    /// Property for `leadingAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeLeading = property(with: view.safeAreaLayoutGuide.leadingAnchor)
    /// Property for `trailingAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeTrailing = property(with: view.safeAreaLayoutGuide.trailingAnchor)
    /// Property for `widthAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeWidth = dimensionProperty(with: view.safeAreaLayoutGuide.widthAnchor)
    /// Property for `heightAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeHeight = dimensionProperty(with: view.safeAreaLayoutGuide.heightAnchor)
    /// Property for `centerXAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeCenterX = property(with: view.safeAreaLayoutGuide.centerXAnchor)
    /// Property for `centerYAnchor`. It's related to `safeAreaLayoutGuide`.
    public lazy var safeCenterY = property(with: view.safeAreaLayoutGuide.centerYAnchor)

    private let view: UIView

    init(view: UIView) {
        self.view = view
    }

    private func property<A: LayoutAnchor>(with anchor: A) -> LayoutAnchorProperty<A> {
        return LayoutAnchorProperty(anchor: anchor)
    }

    private func dimensionProperty<A: LayoutDimension>(with anchor: A) -> LayoutDimensionProperty<A> {
        return LayoutDimensionProperty(anchor: anchor)
    }

    /// Fills the source view to it's superview with insets. Also you can specify if you want it to be relative to it's superview safe area or not.
    ///
    /// Please use
    ///
    /// ```
    /// view.makeLayout {
    ///     $0.fillSuperview()
    /// }
    /// ```
    ///
    /// to call this method as `makeLayout` setup `translatesAutoresizingMaskIntoConstraints = false`.
    ///
    /// Otherwise you can do
    ///
    /// ```
    /// view.translatesAutoresizingMaskIntoConstraints = false
    /// view.layout.fillSuperview()
    /// ```
    ///
    public func fillSuperview(with margins: UIEdgeInsets = .zero, relativeToSafeArea: Bool = false) {
        guard let superview = view.superview else { return }
        if relativeToSafeArea {
            top.equal(to: superview.layout.safeTop, offsetBy: margins.top)
            left.equal(to: superview.layout.safeLeft, offsetBy: margins.left)
            bottom.equal(to: superview.layout.safeBottom, offsetBy: -margins.bottom)
            right.equal(to: superview.layout.safeRight, offsetBy: -margins.right)
        } else {
            top.equal(to: superview.layout.top, offsetBy: margins.top)
            left.equal(to: superview.layout.left, offsetBy: margins.left)
            bottom.equal(to: superview.layout.bottom, offsetBy: -margins.bottom)
            right.equal(to: superview.layout.right, offsetBy: -margins.right)
        }
    }

    /// Setup an aspect ratio.
    ///
    /// Please use
    ///
    /// ```
    /// view.makeLayout {
    ///     $0.aspectRatio(1.5)
    /// }
    /// ```
    ///
    /// to call this method as `makeLayout` setup `translatesAutoresizingMaskIntoConstraints = false`.
    ///
    /// Otherwise you can do
    ///
    /// ```
    /// view.translatesAutoresizingMaskIntoConstraints = false
    /// view.layout.aspectRatio(1.5)
    /// ```
    ///
    public func aspectRatio(_ ratio: CGFloat = 1) {
        width.equal(to: view.layout.height, multiplier: ratio)
    }

    /// Center the source view into the target view.
    ///
    /// Please use
    ///
    /// ```
    /// view.makeLayout {
    ///     $0.center(targetView)
    /// }
    /// ```
    ///
    /// to call this method as `makeLayout` setup `translatesAutoresizingMaskIntoConstraints = false`.
    ///
    /// Otherwise you can do
    ///
    /// ```
    /// view.translatesAutoresizingMaskIntoConstraints = false
    /// view.layout.center(targetView)
    /// ```
    ///
    public func center(_ otherView: UIView) {
        centerX.equal(to: otherView.layout.centerX)
        centerY.equal(to: otherView.layout.centerY)
    }

    /// Center the source view into it's superview.
    ///
    /// Please use
    ///
    /// ```
    /// view.makeLayout {
    ///     $0.centerSuperview()
    /// }
    /// ```
    ///
    /// to call this method as `makeLayout` setup `translatesAutoresizingMaskIntoConstraints = false`.
    ///
    /// Otherwise you can do
    ///
    /// ```
    /// view.translatesAutoresizingMaskIntoConstraints = false
    /// view.layout.centerSuperview()
    /// ```
    ///
    public func centerSuperview() {
        guard let superview = view.superview else { return }
        center(superview)
    }
}

public extension UIView {

    /// Layout proxy for the current view. This can be used to create constraints with operators. If the view is the source view, you should use `makeLayout { ... }` method instead, as this sets the property `translatesAutoresizingMaskIntoConstraints = false`.
    var layout: LayoutProxy {
        return LayoutProxy(view: self)
    }

    /// This method provides the layout proxy and setup `translatesAutoresizingMaskIntoConstraints = false`.
    /// Inside the provided block you can create all the constraints using the property operators to create them.
    ///
    /// Example:
    ///
    /// ```
    /// textLabel.makeLayout {
    ///     $0.top == titleLabel.layout.bottom + 20
    ///     $0.centerX <= titleLabel.layout.centerX
    ///     $0.width == titleLabel.layout.width + 10
    ///     $0.height >= titleLabel.layout.height
    /// }
    /// ```
    ///
    /// Anchors (like `top`, `left`, `right`, `bottom`, `centerX`, `centerY`, `leading`, `trailing`) and Dimensions (like `width`, `height`) can be related with `==`, `>=` and `<=`.
    ///
    /// Additionally, you can add an offset with +/- at the end.
    ///
    /// For Dimensions, there's also the option to assign it directly to a value like this:
    ///
    /// ```
    /// textLabel.makeLayout {
    ///     $0.width == 10
    /// }
    /// ```
    ///
    /// Or even create a multiplier relation like this:
    ///
    /// ```
    /// textLabel.makeLayout {
    ///     $0.width == titleLabel.layout.width * 3
    ///     $0.height == titleLabel.layout.height * 2 + 10
    /// }
    /// ```
    ///
    /// - Parameter closure: The block with the proxy as a parameter. Inside this block you should do all the layout constructions
    func makeLayout(closure: (LayoutProxy) -> Void) {
        translatesAutoresizingMaskIntoConstraints = false
        closure(LayoutProxy(view: self))
    }
}
