//  Created by Albert Montserrat on 25/07/2019.
//  Copyright © 2019 FiNetwork. All rights reserved.
//  All Credits to: https://github.com/AlbertMontserrat/AMGAutolayout

import UIKit

public protocol LayoutAnchor {
    func constraint(equalTo anchor: Self, constant: CGFloat) -> NSLayoutConstraint
    func constraint(greaterThanOrEqualTo anchor: Self, constant: CGFloat) -> NSLayoutConstraint
    func constraint(lessThanOrEqualTo anchor: Self, constant: CGFloat) -> NSLayoutConstraint
}

extension NSLayoutAnchor: LayoutAnchor {}
