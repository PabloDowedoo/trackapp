//
//  BottomSheetViewController.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import TrackAppCommon

final class BottomSheetViewController: UIViewController, UIGestureRecognizerDelegate {

    // MARK: - UI
    private let topCornersView = UIView()

    // MARK: - StoredProperties
    private var disposeBag = DisposeBag()

    // MARK: - Injected Properties
    var onEmptyViewTap: VoidClosure?

    init(embeddedView: UIView) {
        super.init(nibName: nil, bundle: nil)
        setupView(embeddedView: embeddedView)
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}

private extension BottomSheetViewController {
    func setupView(embeddedView: UIView) {
        setupTapOnEmptyView()
        roundTopCorners()

        topCornersView.backgroundColor = Palette.background.primary
        topCornersView.addSubview(embeddedView)
        embeddedView.makeLayout {
            $0.fillSuperview()
        }

        view.addSubview(topCornersView)

        topCornersView.makeLayout {
            $0.left == view.layout.left
            $0.bottom == view.layout.bottom
            $0.right == view.layout.right
        }
    }

    func setupTapOnEmptyView() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
        tapGesture.rx.event
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] _ in
                self?.onEmptyViewTap?()
        }).disposed(by: disposeBag)
    }

    func roundTopCorners() {
        topCornersView.layer.masksToBounds = true
        topCornersView.layer.cornerRadius = 6
        topCornersView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}
