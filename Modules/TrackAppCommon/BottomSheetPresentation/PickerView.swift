//
//  PickerView.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public struct PickerDatasource {
    public let id: String
    public let name: String

    public init(id: String,
                name: String) {
        self.id = id
        self.name = name
    }
}

public struct PickerViewModel {
    public let datasource: [PickerDatasource]
    public let selectedId: String?

    public init(datasource: [PickerDatasource],
                selectedId: String?) {
        self.datasource = datasource
        self.selectedId = selectedId
    }
}

final class PickerView: UIView {

    // MARK: - UI
    private lazy var picker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    private let done = UIButton(type: .system)

    // MARK: - StoredProperties
    private var disposeBag = DisposeBag()
    private var pickerData: [PickerDatasource] = [] {
        didSet {
            picker.reloadAllComponents()
        }
    }

    // MARK: - Injected Properties
    var onSelectedRow: ((String) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(_ viewModel: PickerViewModel) {
        pickerData = viewModel.datasource

        let initialSelectedIndex = viewModel.datasource
            .enumerated()
            .first (where: {
                $0.element.id == viewModel.selectedId
            })?.offset ?? 0

        let itemSelected = picker.rx.itemSelected
            .startWith((row: initialSelectedIndex, component: 0))

        picker.selectRow(initialSelectedIndex, inComponent: 0, animated: false)

        done.rx.tap
            .withLatestFrom(itemSelected)
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] data in
                guard let self = self else { return }
                self.onSelectedRow?(self.pickerData[data.row].id)
            }).disposed(by: disposeBag)
    }
}

extension PickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].name
    }
}

private extension PickerView {
    func setupView() {
        done.setTitle(Constants.doneTitle, for: .normal)

        let topView = UIStackView(views: [UIView(), done], axis: .horizontal)
        let contentContainer = UIStackView(views: [topView, picker], axis: .vertical)
        addSubview(contentContainer)
        contentContainer.makeLayout {
            $0.fillSuperview(with: UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 16))
        }
    }

    enum Constants {
        static var doneTitle: String { "Done" }
    }
}

public extension UIViewController {
    func presentPickerView(viewModel: PickerViewModel, onSelectedRow: @escaping (String) -> Void) {
        var transitionDelegate: UIViewControllerTransitioningDelegate? = SlideInPresentationManager(direction: .bottom)

        let pickerView = PickerView()
        pickerView.bind(viewModel)

        let bottomSheetVC = BottomSheetViewController(embeddedView: pickerView)
        bottomSheetVC.transitioningDelegate = transitionDelegate
        bottomSheetVC.modalPresentationStyle = .custom

        pickerView.onSelectedRow = { [weak bottomSheetVC] in
            onSelectedRow($0)
            bottomSheetVC?.dismiss(animated: true, completion: { transitionDelegate = nil})
        }

        bottomSheetVC.onEmptyViewTap = { [weak bottomSheetVC] in
            bottomSheetVC?.dismiss(animated: true, completion: { transitionDelegate = nil})
        }

        self.present(bottomSheetVC, animated: true, completion: nil)
    }
}
