//
//  PaletteType.swift
//  TrackApp
//
//  Created by Pablo Sanchez Gomez on 26/04/2020.
//  Copyright © 2020 Pablo Sanchez Gomez. All rights reserved.
//

import UIKit

public struct Palette {
    public static let text = UIColor.Text(primary: .black,
                                          secondary: .gray)

    public static let utility = UIColor.Utility(income: .green,
                                                expense: .red,
                                                separatorView: .gray)

    public static let background = UIColor.Background(primary: .white,
                                                      secondary: .lightGray)
}

public extension UIColor {
    struct Text {
        public let primary: UIColor
        public let secondary: UIColor
    }
    
    struct Background {
        public let primary: UIColor
        public let secondary: UIColor
    }

    struct Utility {
        public let income: UIColor
        public let expense: UIColor
        public let separatorView: UIColor
    }
}
